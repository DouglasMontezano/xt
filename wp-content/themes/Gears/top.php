<div id="masthead">
    <div id="blogname">
        <h1><a href="<?php bloginfo('siteurl'); ?>/" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
        <!-- <h1><a href="<//?php bloginfo('siteurl'); ?>/" title="<//?php bloginfo('name'); ?>"></a></h1> -->

        <!-- mostra o logo caso tenha -->
        <?php
        $custom_logo_id = get_theme_mod('custom_logo');

        $logo = wp_get_attachment_image_src($custom_logo_id, 'full');

        if (has_custom_logo()) {
            echo '<img src="' . esc_url($logo[0]) . '"class="alignleftop img-fluid">';
        } else {
            echo '<h1>' . get_blog_info('name') . '</h1>';
        }
        ?>
    </div>
    <div id="head">
        <div id="top" class="clearfix">
            <!-- blogname originalmente Aqui -->
            <div id="contactlist">
                <ul>
                    <li>
                        <span>
                            <//?php $my_name=get_option('gear_my_name'); echo $my_name ?>
                        </span>
                    </li>
                    <li>
                        <span>
                            <?php $my_email = get_option('gear_my_email');
                            echo $my_email ?>
                        </span>
                    </li>
                    <li>
                        <span>
                            <?php $my_phone = get_option('gear_my_phone');
                            echo $my_phone ?>
                        </span>
                    </li>
                    <li>
                        <span>
                            <?php $my_twit = get_option('gear_my_twit');
                            echo $my_twit ?>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        <div id="botmenu">
            <?php wp_nav_menu(array('container_id' => 'submenu', 'theme_location' => 'primary', 'menu_class' => 'sfmenu', 'fallback_cb' => 'fallbackmenu')); ?>
            <?php include TEMPLATEPATH . '/searchform.php'; ?>
        </div>
        <!-- END botmenu -->
    </div>
</div>
<!--end masthead--> 