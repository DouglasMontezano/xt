<?php

/* Cars listing*/

function post_type_cars()
{
 register_post_type(
  'cars',
  array('public' => true,
   'publicly_queryable' => true,
   'has_archive' => true,
   'hierarchical' => false,
   'menu_icon' => get_stylesheet_directory_uri() . '/images/car.png',
   'labels' => array(
    'name' => _x('Carros', 'post type general name'),
    'singular_name' => _x('Carro', 'post type singular name'),
    'add_new' => _x('Adicionar novo', 'car'),
    'add_new_item' => __('Adicionar novo carro'),
    'edit_item' => __('Editar Carrro'),
    'new_item' => __('Novo Carro'),
    'view_item' => __('Visualizar carro'),
    'search_items' => __('Pesquisar carros'),
    'not_found' => __('Nenhum carro encontrado'),
    'not_found_in_trash' => __('Nenhum carro encontrado em lixo'),
    'parent_item_colon' => '',
   ),
   'show_ui' => true,
   'menu_position' => 5,
   'query_var' => true,
   'rewrite' => true,
   'rewrite' => array('slug' => 'car', 'with_front' => false),
   'register_meta_box_cb' => 'mytheme_add_box',
   'supports' => array(
    'title',
    'thumbnail',
    'custom-fields',
    'comments',
    'editor',
   ),
  )
 );
}
add_action('init', 'post_type_cars');

/* Price range taxonomy */

function create_car_price_taxonomy()
{
 $labels = array(
  'name' => _x('Faixa de Preço', 'taxonomy general name'),
  'singular_name' => _x('Faixa de Preço', 'taxonomy singular name'),
  'search_items' => __('Pesquisar Faixa de Preço'),
  'all_items' => __('Toda Faixa de Preço'),
  'parent_item' => __('Faixa de preço dos pais'),
  'parent_item_colon' => __('Faixa de preço dos pais:'),
  'edit_item' => __('Editar faixa de preço'),
  'update_item' => __('Editar Faixa de preço'),
  'add_new_item' => __('Adicionar Nova Faixa de preço'),
  'new_item_name' => __('Novo nome de Faixa de Preço'),
 );
 register_taxonomy('car_price', array('cars'), array(
  'hierarchical' => true,
  'labels' => $labels,
  'show_ui' => true,
  'query_var' => true,
  'rewrite' => array('slug' => 'price-range'),
 ));
}

/* Model Year Taxonomy */

function create_model_year_taxonomy()
{
 $labels = array(
  'name' => _x('Ano', 'taxonomy general name'),
  'singular_name' => _x('Ano', 'taxonomy singular name'),
  'search_items' => __('Pesquisar Ano'),
  'all_items' => __('Todos os Anos'),
  'parent_item' => __('Parent Ano'),
  'parent_item_colon' => __('Parent Ano:'),
  'edit_item' => __('Editar Ano'),
  'update_item' => __('Atualizar Ano'),
  'add_new_item' => __('Adicionar Novo Ano'),
  'new_item_name' => __('Novo Ano'),
 );
 register_taxonomy('model_year', array('cars'), array(
  'hierarchical' => true,
  'labels' => $labels,
  'show_ui' => true,
  'query_var' => true,
  'rewrite' => array('slug' => 'year'),
 ));

}

/* Car Condition Taxonomy */

function create_condition_taxonomy()
{
 $labels = array(
  'name' => _x('Condição', 'taxonomy general name'),
  'singular_name' => _x('Condição', 'taxonomy singular name'),
  'search_items' => __('Pesquisar Condição'),
  'all_items' => __('Todas Condições'),
  'parent_item' => __('Parent Condição'),
  'parent_item_colon' => __('Parent Condição'),
  'edit_item' => __('Editar Condição'),
  'update_item' => __('Atualizar Condição'),
  'add_new_item' => __('Adicionar Condição'),
  'new_item_name' => __('Nova Condição'),
 );
 register_taxonomy('condition', array('cars'), array(
  'hierarchical' => true,
  'labels' => $labels,
  'show_ui' => 'radio',
  'query_var' => true,
  'rewrite' => array('slug' => 'condition'),
 ));

}

/* Make Taxonomy */

function create_make_taxonomy()
{
 $labels = array(
  'name' => _x('Marca', 'taxonomy general name'),
  'singular_name' => _x('Marca', 'taxonomy singular name'),
  'search_items' => __('Pesquisar Marcas'),
  'all_items' => __('All Makes'),
  'parent_item' => __('Parent Make'),
  'parent_item_colon' => __('Parent Make'),
  'edit_item' => __('Editar Marca'),
  'update_item' => __('Atualizar Marca'),
  'add_new_item' => __('Adicionar Marca'),
  'new_item_name' => __('Nova Marca'),
 );
 register_taxonomy('make', array('cars'), array(
  'hierarchical' => true,
  'labels' => $labels,
  'show_ui' => true,
  'query_var' => true,
  'rewrite' => array('slug' => 'make'),
 ));

}

/* Body type Taxonomy */

function create_body_taxonomy()
{
 $labels = array(
  'name' => _x('Tipo Carroceria', 'taxonomy general name'),
  'singular_name' => _x('Tipo Carroceria', 'taxonomy singular name'),
  'search_items' => __('Pesquisar Tipo Carroceria'),
  'all_items' => __('Todos Tipos Carrocerias'),
  'parent_item' => __('Parent Tipo Carroceria'),
  'parent_item_colon' => __('Parent Tipo Carroceria'),
  'edit_item' => __('Editar Tipo Carroceria'),
  'update_item' => __('Atualizar Tipo Carroceria'),
  'add_new_item' => __('Adicionar Tipo Carroceria'),
  'new_item_name' => __('Novo Tipo Carroceria'),
 );
 register_taxonomy('body', array('cars'), array(
  'hierarchical' => true,
  'labels' => $labels,
  'show_ui' => true,
  'query_var' => true,
  'rewrite' => array('slug' => 'body-type'),
 ));

}

add_action('init', 'create_car_price_taxonomy', 0);
add_action('init', 'create_model_year_taxonomy', 0);
add_action('init', 'create_condition_taxonomy', 0);
add_action('init', 'create_make_taxonomy', 0);
add_action('init', 'create_body_taxonomy', 0);

/* PRE-DEFINE TERMS */

##New##
function add_custom_term_new()
{
 if (!is_term('New', 'condition')) {
  wp_insert_term('New', 'condition');
 }
}

##Used#
function add_custom_term_old()
{
 if (!is_term('Used', 'condition')) {
  wp_insert_term('Used', 'condition');
 }
}

add_action('init', 'add_custom_term_new');
add_action('init', 'add_custom_term_old');
