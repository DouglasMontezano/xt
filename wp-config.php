<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'xtveiculos');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

define('WPLANG', 'pt_BR');


/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a*qOA]tCbVU]lg<65d%$jby/{>FZb=P=:@/LZ8?(7(D?o!(q*s<s.us<fkTa1>HT');
define('SECURE_AUTH_KEY',  'L/wcPMuQ}fQCn1Cn-ZYW;M,cFmZ9O6rD@66(&;;J Cz13HI~MRlO$P2/jsgb +?J');
define('LOGGED_IN_KEY',    'Z^~7.`AVfddzNTS&^}!j/q9wg4U~4ib0zf;-@HrO$+O#f[PVS$|&!xg9EXcAvt@X');
define('NONCE_KEY',        'mJD[rpe`&7A&C7N^|oU0sH1r_&Z7UqchgYCE@b(itYN)%dc@jI*W` =QF1C6:/!0');
define('AUTH_SALT',        '%lH0@-H;N-E!CiPUt95>]qs}Y7?2:%Us;|f@U?)9s0E/&_)I[[-qdv$WF;s5>!bQ');
define('SECURE_AUTH_SALT', 'd_Vfybe#~KrKtDagtU)2gOPJn^,q/2wMQWnm9TWW:O(/5^JUxCYaTP%U;FYc*RjR');
define('LOGGED_IN_SALT',   'LY2*3QUTE;7FNA,lWJh>V??~)~c$1gw4;&eCAvs_Dejck{u+b6vIB-Kz{6d[0Z7M');
define('NONCE_SALT',       'Me|0T$pci7}t^DoM}9y7DA%_aY6L,C{Da3wvk~MtVdw?20dbtyq+cD f$*PG-{(!');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
